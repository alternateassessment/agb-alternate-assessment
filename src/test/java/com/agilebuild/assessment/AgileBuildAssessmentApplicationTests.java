package com.agilebuild.assessment;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.agilebuild.assessment.model.Movie;
import com.agilebuild.assessment.repository.MovieRepository;
import com.agilebuild.assessment.service.MovieService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
public class AgileBuildAssessmentApplicationTests {
	

	private Movie heaven, limelight, wings;
	 
	private Answer<Movie> withMovieById = new Answer<Movie>() {
		@Override
		public Movie answer(InvocationOnMock invocation) throws Throwable {
			Object[] args = invocation.getArguments();
			int id = ((Long)args[1]).intValue(); // Cast to int for switch.
			switch (id) {
			case 1 : return heaven;
			case 2 : return limelight;
			case 3 : return wings;
			default : return null;
			}
		}
	};
	
	@Autowired
	private MovieService service;

	@MockBean
	private MovieRepository mockRepository;

	@Before
	public void setUp() throws Exception {
		setupMovies();
	}

	private void setupMovies() {
		heaven = new Movie("Heaven", "4", "Romantic");
		limelight = new Movie("Lime Light", "3", "Science");
		wings = new Movie("WIngs", "5", "Fiction");		
	}
	
	@Test
	public void getAllMoviesTest() {
		// Given
		Mockito.when(mockRepository.findAll()).thenReturn(Stream.of(heaven, limelight).collect(Collectors.toList()));
		
		// When
		List<Movie> actualMovies = service.getAllMovies();
		
		// Then
		assertEquals(2, actualMovies.size());	
	}
	
	@Test
	public void getMovieByIdTest() {
		//Given
		long id = 1;
		Mockito.<Optional<Movie>>when(mockRepository.findById(id)).thenReturn(Optional.of(limelight));
		
		// When
		Movie actualMovie = service.getMovieById(id);
		
		//Then
		assertEquals(limelight.getId(), actualMovie.getId());
		assertEquals(limelight.getGenre(), actualMovie.getGenre());			
	}
	
	@Test
	public void updateMovieTest() {
		//Given
		Movie inputMovie = new Movie("Lime Light", "3", "Science");
		Movie outputMovie = new Movie("Lime Light", "4", "Science");
		Mockito.when(mockRepository.save(inputMovie)).thenReturn(outputMovie);
		
		//When
		Movie output = service.saveMovie(inputMovie);
		
		//Then
		assertEquals(output.getRating(),outputMovie.getRating());		
		assertTrue("Rating of Input Movie is different from Output Movie", output.getRating()!=inputMovie.getRating());
	}
	
	@Test
	public void saveMovieTest() {
		// Given
		Movie inputMovie = new Movie("WIngs", "5", "Fiction");
		Mockito.when(mockRepository.save(inputMovie)).thenReturn(inputMovie);
		
		//When
		Movie output= service.saveMovie(inputMovie);
		
		//Then
		assertEquals(output.getTitle(),inputMovie.getTitle());
	}
}
