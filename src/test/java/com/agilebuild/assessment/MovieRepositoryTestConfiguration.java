package com.agilebuild.assessment;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import com.agilebuild.assessment.repository.MovieRepository;
import com.agilebuild.assessment.service.MovieService;

@Profile("test")
@Configuration
public class MovieRepositoryTestConfiguration {
	@Bean
	@Primary
	public MovieRepository movieRepository() {
		return Mockito.mock(MovieRepository.class);
	}
}
