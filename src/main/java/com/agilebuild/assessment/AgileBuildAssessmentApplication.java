package com.agilebuild.assessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgileBuildAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgileBuildAssessmentApplication.class, args);
	}

}
