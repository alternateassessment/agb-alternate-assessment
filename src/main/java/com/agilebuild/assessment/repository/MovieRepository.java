package com.agilebuild.assessment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.agilebuild.assessment.model.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie,Long>{

}
