package com.agilebuild.assessment.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agilebuild.assessment.model.Movie;
import com.agilebuild.assessment.repository.MovieRepository;


@Service
public class MovieServiceImpl implements MovieService {

	@Autowired
	private MovieRepository movieRepository;
	
	@Override
	public List<Movie> getAllMovies() {
		return movieRepository.findAll();
	}
	
	@Override
	public Movie saveMovie(Movie movie) {
	Movie savedEntity = this.movieRepository.save(movie);
	return savedEntity;
	}
	
	@Override
	public Movie getMovieById(long id) {
		Optional<Movie> optional = movieRepository.findById(id);
		Movie movie = null;
		if (optional.isPresent()) {
			movie = optional.get();
		} else {
			throw new RuntimeException(" Movie not found for id :: " + id);
		}
		return movie;
	}
	
	@Override
	public void deleteMovieById(long id) {
		this.movieRepository.deleteById(id);
	}
}
