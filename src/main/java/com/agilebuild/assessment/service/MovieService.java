package com.agilebuild.assessment.service;

import java.util.List;

import com.agilebuild.assessment.model.Movie;

public interface MovieService {
List<Movie> getAllMovies();
Movie saveMovie(Movie movie);
Movie getMovieById(long id);
void deleteMovieById(long id);
}
